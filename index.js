const express = require("express");
const app = express();
const lagu = require("./routes/lagu");
const ospot = require("./routes/ospot");
const posts = require("./routes/posts");
const users = require("./routes/users");


app.use(express.json());
app.use("/api/lagu", lagu);
app.use("/api/ospot", ospot);
app.use("/api/posts", posts);
app.use("/api/users", users);

app.listen(3000, function () {
    console.log("Listening on port 3000...");
});