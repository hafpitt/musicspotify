const express = require("express");
const router = express.Router();
const mysql = require("mysql");
const jwt = require("jsonwebtoken");
const yup = require("yup");

const pool = mysql.createPool({
    connectionLimit : 10,
    host : "localhost",
    database : "wsifm20191_lat_uas",
    user: "root",
    password: "root"
});

const schemainput = yup.object().shape({
    title: yup.string().required(),
    content: yup.string().required()
})

const schemaadmin = yup.object().shape({
    password: yup.string().required().length(6),
    is_admin: yup.number().required().oneOf([0,1])
})


//No 1
router.get("/all", function(req, res){
    pool.getConnection(function(err,conn){
        if(err) throw err;
        const q = `select * from posts`
        conn.query(q, function(err,result){
        if(err) throw err;
        if(result.length<=0){
            return res.status(400).send("No Data Found");
        }else{
            conn.release();
            res.send(result);
        }
        });
    });    
});

//No 2 - 3
router.get("/:id", function(req, res){
    pool.getConnection(function(err,conn){
        if(err) throw err;
        const q = `select * from posts where id = '${req.params.id}'`
        conn.query(q, function(err,result){
        if(err) throw err;
        if(result.length<=0){
            return res.status(400).send("No Data Found");
        }else{
            conn.release();
            res.send(result);
        }
        });
    });    
});

//No 4 - 5
router.get("/", function(req, res){
    pool.getConnection(function(err,conn){
        if(err) throw err;
        const q = `select * from posts where title like  '%${req.query.title}%' `
        conn.query(q, function(err,result){
        if(err) throw err;
        if(result.length<=0){
            return res.status(400).send("No Data Found");
        }else{
            conn.release();
            res.send(result);
        }
        });
    });    
});


//No 10 - 13
router.post("/", function(req, res){
    const token = req.header("x-auth-token");
    if(!token){
        return res.status(401).send("No token found");
    }
    let user = {};
    try{
        user = jwt.verify(token, "12345678")
    }catch(ex){
        return res.status(400).send("No token found");
    }
    console.log(user);
    
        pool.getConnection(async function(err, conn){
            
            try{    
            const inputpost = await schemainput.validate(req.body, {abortEarly:false});
            if(err) throw err;
            const q = `insert into posts (title,content,username) values ('${req.body.title}','${req.body.content}','${user.username}')`
            conn.query(q, function(err, result){
                conn.release();
                if(err) throw err;
                res.send(result);
            })
            }catch(err){
                res.status(400).send("posting_isi is empty");
            }
        })
});

//No 14 - 19
router.put("/:username", function(req, res){
    const token = req.header("x-auth-token");
    if(!token){
        return res.status(401).send("No token found");
    }
    let user = {};
    try{
        user = jwt.verify(token, "12345678")
    }catch(ex){
        return res.status(400).send("No token found");
    }
    if(user.is_admin != 1){
        return res.status(403).send("You are not allowed resource");
    }
    console.log(user);

    pool.getConnection(async function(err, conn){
            
        try{    
        const putadmin = await schemaadmin.validate(req.body, {abortEarly:false});
        if(err) throw err;
        const q = `update from users set password = '${req.body.password}' and is_admin = ${req.body.is_admin} where username = '${req.params.username}'`
        conn.query(q, function(err, result){
            conn.release();
            if(err) throw err;
            res.send(result);
        })
        }catch(err){
            res.status(400).send("Parameter not valid");
        }
    })

});

module.exports = router;