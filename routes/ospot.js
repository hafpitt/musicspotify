const express = require("express");
const router = express.Router();
const mysql = require("mysql");
const jwt = require("jsonwebtoken");
const yup = require("yup");
const request = require("request");


router.post("/create", function (req,res){
    const headers = {
        'Accept' : 'application/json',
        'Content-Type' : 'application/json',
        'Authorization' : 'Bearer *****************************'
    }
	request.post({url:"https://api.spotify.com/v1/users/*************************/playlists", headers:headers, form: JSON.stringify(req.body)},
		function (error,response,body){
			if(error){
				console.log(error);
				res.status(500).send("Error saat akses spotify");
			}
			else{
				const hasil = JSON.parse(body);
				// console.log(`Film ${hasil["Title"]} yang dirilis pada tahun ${hasil["Year"]} memperoleh rating ${hasil["imdbRating"]} di IMDB`);
				res.send(hasil);
			}
		}
	)
});

module.exports = router;
