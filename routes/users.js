const express = require("express");
const router = express.Router();
const mysql = require("mysql");
const jwt = require("jsonwebtoken");
const yup = require("yup");



const pool = mysql.createPool({
    connectionLimit : 10,
    host : "localhost",
    database : "dbmusic",
    user: "root",
    password: "root"
});

const schema = yup.object().shape({
    username: yup.string().required().min(5),
    password: yup.string().required().min(5)
})

const schemaadmin = yup.object().shape({
    // password: yup.string().required().length(6),
    // is_admin: yup.number().required().oneOf([0,1])
})

//No 6 - 7
router.post("/register", async function(req, res){
    pool.getConnection(async function(err,conn){
        if(err) throw err;
        try{    
        const inputuser = await schema.validate(req.body, {abortEarly:false});
        const query = `insert into users values ('${req.body.username}','${req.body.password}', 0)`;
        conn.query(query,function(err,result){
            conn.release();
            if(err) throw err;
            res.status(201).send({"username":req.body.username, "is_admin":"0"});
        })
        }catch(err){
            res.status(400).send("Input Value not Valid");
        }
    })
});

//No 8 - 9
router.post("/login", function(req, res){
    pool.getConnection(async function(err, conn){
    
    if(err) throw err;
        const q = `select * from users where username = '${req.body.username}' and password = '${req.body.password}'`;
        conn.query(q, async function(err, result){
            conn.release();
            if(err) throw err;
            if (result.length<=0){
                res.status(400).send("Invalid Username or Password");
            }else{
                const user = result[0];
                   
                    const token = jwt.sign({
                        "username":user.username,
                        "is_admin":user.is_admin  
                    }, '12345678');
                    return res.status(201).send(token);          
            }
        })
    })
});

//No 14 - 19
router.put("/:username", function(req, res){
    const token = req.header("x-auth-token");
    if(!token){
        return res.status(401).send("No token found");
    }
    let user = {};
    try{
        user = jwt.verify(token, "12345678")
    }catch(ex){
        return res.status(400).send("No token found");
    }
    if(user.is_admin != 1){
        return res.status(403).send("You are not allowed resource");
    }
    console.log(user);

    pool.getConnection(async function(err, conn){
            
        try{    
        const putadmin = await schemaadmin.validate(req.body, {abortEarly:false});
        if(err) throw err;
        const q = `update users set password = '${req.body.password}', is_admin = '${req.body.is_admin}' where username = '${req.params.username}'`
        conn.query(q, function(err, result){
            conn.release();
            if(err) throw err;
            res.send(result);
        })
        }catch(err){ 
            res.status(400).send("Parameter not valid");
        }
    })
});

router.delete("/:username", function(req, res){
    const token = req.header("x-auth-token");
    if(!token){
        return res.status(401).send("No token found");
    }
    let user = {};
    try{
        user = jwt.verify(token, "12345678")
    }catch(ex){
        return res.status(400).send("No token found");
    }
    if(user.is_admin != 1){
        return res.status(403).send("You are not allowed resource");
    }
    console.log(user);

    pool.getConnection(async function(err, conn){
            
        if(err) throw err;
        const q = `delete from users where username = '${req.params.username}'`
        conn.query(q, function(err, result){
            conn.release();
            if(err) throw err;
            res.send("berhasil hore");
        })
    })
});

module.exports = router;